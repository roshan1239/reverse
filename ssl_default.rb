secret = Chef::EncryptedDataBagItem.load_secret("/tmp/my_secret_key")
item = Chef::EncryptedDataBagItem.load('sslcerts', 'node2', secret)

item['certs'].each do |cert|
	file "/tmp/#{cert['filename']}" do
		content cert['data']
		owner 'steve'
	end
end
yum remove httpd httpd-devel httpd-manual httpd-tools mod_auth_kerb mod_auth_mysql mod_auth_pgsql mod_authz_ldap mod_dav_svn mod_dnssd mod_nss mod_perl mod_revocator mod_ssl mod_wsgi
