# reverse
httpd.conf-

LoadModule proxy_module modules/mod_proxy.so
LoadModule proxy_http_module modules/mod_proxy_http.so



<VirtualHost *:80>
# Your domain name
ServerName ec2-34-212-138-210.us-west-2.compute.amazonaws.com

ProxyPreserveHost Off


SSLProtocol all -SSLv2 -SSLv3

ProxyPass / http://ec2-34-212-138-210.us-west-2.compute.amazonaws.com:9000/
ProxyPassReverse / http://ec2-34-212-138-210.us-west-2.compute.amazonaws.com:9000/
</VirtualHost>



/etc/httpd/conf
